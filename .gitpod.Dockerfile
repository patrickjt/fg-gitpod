FROM gitpod/workspace-full-vnc

RUN sudo apt-get update

# Hide the keyboard prompt
RUN echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections

# Run download_and_compile
RUN sudo apt-get install -y dctrl-tools build-essential libarchive-dev libbz2-dev libexpat1-dev libjsoncpp-dev liblzma-dev libncurses5-dev libssl-dev procps zlib1g-dev cmake libboost-dev libcgal-dev libgdal-dev libtiff5-dev freeglut3-dev libglew-dev libopenal-dev libboost-dev libopenscenegraph-dev libudev-dev libdbus-1-dev libplib-dev qt5-default qtdeclarative5-dev qttools5-dev qtbase5-dev-tools qttools5-dev-tools qml-module-qtquick2 qml-module-qtquick-window2 qml-module-qtquick-dialogs qml-module-qtquick-controls2 libqt5opengl5-dev libqt5svg5-dev libqt5websockets5-dev qtbase5-private-dev qtdeclarative5-private-dev fluid libbz2-dev libfltk1.3-dev libxi-dev libxmu-dev libxinerama-dev libjpeg-dev libxft-dev libevent-dev python3-pyqt5 python3-pyqt5.qtmultimedia libqt5multimedia5-plugins python-tk